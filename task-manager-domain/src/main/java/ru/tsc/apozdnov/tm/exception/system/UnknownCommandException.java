package ru.tsc.apozdnov.tm.exception.system;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public final class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Error! Command not supported...");
    }

    public UnknownCommandException(final String command) {
        super("Error! Command `" + command + "` not supported...");
    }

}