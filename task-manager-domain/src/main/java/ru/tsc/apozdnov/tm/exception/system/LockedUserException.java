package ru.tsc.apozdnov.tm.exception.system;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public class LockedUserException extends AbstractException {

    public LockedUserException() {
        super("Error! User is locked...");
    }

}