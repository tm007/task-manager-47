package ru.tsc.apozdnov.tm.api.service.model;

import ru.tsc.apozdnov.tm.api.repository.model.IRepository;
import ru.tsc.apozdnov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}