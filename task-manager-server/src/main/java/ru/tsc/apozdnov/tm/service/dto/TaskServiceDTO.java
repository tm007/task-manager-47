package ru.tsc.apozdnov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.tsc.apozdnov.tm.api.service.IConnectionService;
import ru.tsc.apozdnov.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.apozdnov.tm.dto.model.TaskDTO;
import ru.tsc.apozdnov.tm.exception.field.EmptyDescriptionException;
import ru.tsc.apozdnov.tm.exception.field.EmptyIdException;
import ru.tsc.apozdnov.tm.exception.field.EmptyNameException;
import ru.tsc.apozdnov.tm.exception.field.EmptyUserIdException;
import ru.tsc.apozdnov.tm.repository.dto.TaskRepositoryDTO;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public class TaskServiceDTO extends AbstractUserOwnedServiceDTO<TaskDTO, ITaskRepositoryDTO> implements ITaskServiceDTO {

    public TaskServiceDTO(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ITaskRepositoryDTO getRepository(@NotNull final EntityManager entityManager) {
        return new TaskRepositoryDTO(entityManager);
    }

    @Nullable
    @Override
    public TaskDTO create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setUserId(userId);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Nullable
    @Override
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Nullable
    @Override
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            return repository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

}