package ru.tsc.apozdnov.tm.api.service.dto;

import ru.tsc.apozdnov.tm.dto.model.ProjectDTO;

public interface IProjectServiceDTO extends IUserOwnedServiceDTO<ProjectDTO> {

}