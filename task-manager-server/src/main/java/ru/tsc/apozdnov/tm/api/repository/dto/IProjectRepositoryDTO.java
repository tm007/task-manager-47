package ru.tsc.apozdnov.tm.api.repository.dto;

import ru.tsc.apozdnov.tm.dto.model.ProjectDTO;

public interface IProjectRepositoryDTO extends IUserOwnedRepositoryDTO<ProjectDTO> {

}
